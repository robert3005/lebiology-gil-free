#define DOUBLEP 1
#define F77_FUNC(name,NAME) name ## _
#define F77_FUNC_(name,NAME) name ## _
#define HAVE_LIBVTKSYS 1
#define HAVE_LIBVTKCOMMON 1
#define HAVE_LIBVTKFILTERING 1
#define HAVE_LIBVTKGRAPHICS 1
#define HAVE_LIBVTKIO 1
#define HAVE_MPI 1
#define HAVE_MPI_CXX 1
#define _MPI_CPP_BINDINGS 1
