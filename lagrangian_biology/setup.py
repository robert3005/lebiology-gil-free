from distutils.core import setup
from Cython.Build import cythonize
from Cython.Distutils import build_ext, Extension
from Cython.Compiler import Options

# generate annotations
Options.annotate = True
# Don't raise python exceptions on division by 0
# Necessary for gil free execution
# Per module settings are ignored - set globally
Options.directive_defaults["cdivision"] = True
Options.directive_defaults["unraisable_tracebacks"] = False

extensions = [
    Extension("liblebiology", ["lebiology.pyx", "path_ops.cpp", "regex_ops.cpp"]),
]

for e in extensions:
    e.include_dirs = ['.']
    e.language = "c++"
    e.extra_compile_args = ["-fopenmp", "-std=c++11", "-I../include"]
    e.extra_link_args = ["-lgomp", "-lstdc++", "-lboost_filesystem"]

setup(
    name = "LEBiology",
    cmdclass = {"build_ext": build_ext},
    ext_modules = cythonize(extensions)
)
