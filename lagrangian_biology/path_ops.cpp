#include "path_ops.h"

string composePath(vector<string> &pathParts) {
    path p;

    for(auto part : pathParts) {
        p /= part;
    }

    return p.string();
};

string parentPath(const string &p) {
    return path(p).parent_path().string();
};

string absolutePath(const string &prePath) {
    path p(prePath);
    return absolute(p).string();
};

int remove(const string & dir) {
    return remove_all(path(dir));
};

bool create(const string & dir) {
    return create_directory(path(dir));
};

void copy(const string & src, const string & dest) {
    copy(path(src), path(dest));
};

void writeFile(const string & file, const vector<string> & contents) {
    ofstream outFile;
    outFile.open(file);
    for(auto line : contents) {
        outFile << line;
    }
    outFile.close();
};

void writeFile(const string & file, const string & contents) {
    ofstream outFile;
    outFile.open(file);
    outFile << contents;
    outFile.close();
};

string readFile(const string & file) {
    std::ifstream t(file);
    string str;

    t.seekg(0, ios::end);
    str.reserve(t.tellg());
    t.seekg(0, ios::beg);

    str.assign((istreambuf_iterator<char>(t)),
                istreambuf_iterator<char>());

    return str;
};
