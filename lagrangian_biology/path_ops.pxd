from libcpp.vector cimport vector
from libcpp.string cimport string
from libcpp cimport bool

cdef extern from "path_ops.h" nogil:
    string composePath(vector[string] &)
    string parentPath(const string &)
    string absolutePath(const string &)
    int remove(const string &)
    bool create(const string &)
    void copy(const string &, const string &)
    void writeFile(const string &, const vector[string] &)
    void writeFile(const string &, const string &)
    string readFile(const string &)
