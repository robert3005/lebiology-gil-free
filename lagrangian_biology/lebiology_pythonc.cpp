#include "lebiology_pythonc.h"

void lebiology_init(int *stat) {
  initlebiology();
  if(PyErr_Occurred()) {
    PyErr_Print();
    *stat=1;
    return;
  }
}

/****************************
 * Kernel initialisation
 ****************************/

// #ifdef HAVE_PYTHON
// PyObject *create_dict(PyObject *pNameList, double values[], int n_values)
// {
//   /* Create and populate a dict */
//   int i;
//   PyObject *pDict = PyDict_New();
//   for(i=0; i<n_values; i++){
//     PyObject *pValue = PyFloat_FromDouble(values[i]);
//     PyDict_SetItem(pDict, PyList_GET_ITEM(pNameList, i), pValue);
//     Py_DECREF(pValue);
//   }
//   return pDict;
// }

// void add_food_variety(PyObject *pAgent, PyObject *pEnvironment,
// 		      char *fg_key, char *food_name, int n_fvariety,
// 		      double fvariety[], double fingest[])
// {
//   PyObject *pFoodNames = PyDict_GetItemString(pFGFoodNames, fg_key);
//   PyObject *pVarietyNames = PyDict_GetItemString(pFoodNames, food_name);

//   // Add variety concentration to env dict
//   PyObject *pFoodDict = create_dict(pVarietyNames, fvariety, n_fvariety);
//   PyDict_SetItemString(pEnvironment, food_name, pFoodDict);
//   Py_DECREF(pFoodDict);

//   // Add FoodRequest dictionary
//   double *zeroes = (double *)calloc(n_fvariety, sizeof(double));
//   PyObject *pRequestDict = create_dict(pVarietyNames, zeroes, n_fvariety);
//   PyObject *pRequestString = PyString_FromFormat("%s%s", food_name, "Request");
//   PyDict_SetItem(pAgent, pRequestString, pRequestDict);
//   Py_DECREF(pRequestDict);
//   Py_DECREF(pRequestString);

//   // Add FoodThreshold dictionary
//   PyObject *pThresholdDict = create_dict(pVarietyNames, zeroes, n_fvariety);
//   PyObject *pThresholdString = PyString_FromFormat("%s%s", food_name, "Threshold");
//   PyDict_SetItem(pAgent, pThresholdString, pThresholdDict);
//   Py_DECREF(pThresholdDict);
//   Py_DECREF(pThresholdString);
//   free(zeroes);

//   // Add IngestedCells dictionary
//   PyObject *pIngestedDict = create_dict(pVarietyNames, fingest, n_fvariety);
//   PyObject *pIngestedCellsString = PyString_FromFormat("%s%s", food_name, "IngestedCells");
//   PyDict_SetItem(pAgent, pIngestedCellsString, pIngestedDict);
//   Py_DECREF(pIngestedDict);
//   Py_DECREF(pIngestedCellsString);
// }

// void convert_food_variety(PyObject *pResult, char *fg_key, char *food_name,
// 			  int n_fvariety, double frequest[], double fthreshold[])
// {
//   PyObject *pFoodNames = PyDict_GetItemString(pFGFoodNames, fg_key);
//   PyObject *pVarietyNames = PyDict_GetItemString(pFoodNames, food_name);
//   int i;

//   // Convert FoodRequest dictionary
//   PyObject *pRequestName = PyString_FromFormat("%s%s", food_name, "Request");
//   PyObject *pFoodRequests = PyDict_GetItem(pResult, pRequestName);
//   if (!pFoodRequests) {
//     for (i=0; i<n_fvariety; i++) {
//       frequest[i] = 0.0;
//     }
//   } else {
//     for (i=0; i<n_fvariety; i++) {
//       frequest[i] = PyFloat_AsDouble( PyDict_GetItem(pFoodRequests, PyList_GET_ITEM(pVarietyNames, i)) );
//     }
//   }
//   Py_DECREF(pRequestName);

//   // Convert FoodThreshold dictionary
//   PyObject *pThresholdName = PyString_FromFormat("%s%s", food_name, "Threshold");
//   PyObject *pThresholdDict = PyDict_GetItem(pResult, pThresholdName);
//   if (!pThresholdDict) {
//     for (i=0; i<n_fvariety; i++) {
//       fthreshold[i] = 0.0;
//     }
//   } else {
//     for (i=0; i<n_fvariety; i++) {
//       fthreshold[i] = PyFloat_AsDouble( PyDict_GetItem(pThresholdDict, PyList_GET_ITEM(pVarietyNames, i)) );
//     }
//   }
//   Py_DECREF(pThresholdName);
// }
// #endif

// void lebiology_agent_update_c(char *fg, char *key, char *food,
//                               double vars[], int n_vars, double envvals[], int n_envvals,
//                               double fvariety[], double frequest[], double fthreshold[], double fingest[], int n_fvariety,
//                               double *dt, int *dropout, int *stat)
// {
// #ifdef HAVE_PYTHON
//   // Get variable names and local namespace
//   PyObject *pLocalsDict = PyDict_GetItemString(pFGLocalsDict, fg);
//   PyObject *pVarNames = PyDict_GetItemString(pFGVarNames, fg);
//   PyObject *pEnvNames = PyDict_GetItemString(pFGEnvNames, fg);

//   // Get compiled code object from local namespace
//   PyObject *pLocals = PyDict_GetItemString(pLocalsDict, key);
//   PyObject *pFunc = PyDict_GetItemString(pLocals, "val");
//   PyObject *pFuncCode = PyObject_GetAttrString(pFunc, "func_code");
//   int i;

//   // Create agent and environment dict
//   PyObject *pAgent = create_dict(pVarNames, vars, n_vars);
//   PyObject *pEnvironment = create_dict(pEnvNames, envvals, n_envvals);

//   if (n_fvariety > 0) {
//     add_food_variety(pAgent, pEnvironment, fg, food, n_fvariety, fvariety, fingest);
//   }

//   // Create dt argument
//   PyObject *pDt = PyFloat_FromDouble(*dt);

//   // Create args and execute kernel function 'def val(agent, env, dt):'
//   PyObject **pArgs = (PyObject **)malloc(sizeof(PyObject*)*3);
//   pArgs[0] = pAgent;
//   pArgs[1] = pEnvironment;
//   pArgs[2] = pDt;

//   PyObject *pResult = PyEval_EvalCodeEx((PyCodeObject *)pFuncCode, pLocals, NULL, pArgs, 3, NULL, 0, NULL, 0, NULL);

//   // Check for Python errors
//   *stat=0;
//   if(!pResult){
//     PyErr_Print(); *stat=-1; return;
//   }

//   // Convert the python result
//   for(i=0; i<n_vars; i++){
//     vars[i] = PyFloat_AsDouble( PyDict_GetItem(pResult, PyList_GET_ITEM(pVarNames, i)) );
//   }

//   if (n_fvariety > 0) {
//     convert_food_variety(pResult, fg, food, n_fvariety, frequest, fthreshold);
//   }

//   // Check for dropout
//   if (dropout_agent){
//     *dropout = 1;
//     dropout_agent = false;
//   } else {
//     *dropout = 0;
//   }

//   // Check for exceptions
//   if (PyErr_Occurred()){
//     PyErr_Print(); *stat=-1; return;
//   }

//   Py_DECREF(pAgent);
//   Py_DECREF(pEnvironment);
//   Py_DECREF(pDt);
//   Py_DECREF(pFuncCode);
//   Py_DECREF(pResult);
//   free(pArgs);
// #endif
// }


// /*****************************************
//  * Callbacks via Python module "lebiology"
//  *****************************************/

// static PyObject *lebiology_add_agent(PyObject *self, PyObject *args)
// {
// #ifdef HAVE_PYTHON
//   PyObject *pAgentDict, *pPositionList, *pAgentVar;
//   char *fg;
//   if (!PyArg_ParseTuple(args, "sOO", &fg, &pAgentDict, &pPositionList)) {
//     return NULL;
//   }

//   // Check for correctly parsed agent argument
//   if (!pAgentDict) {
//      PyErr_SetString(PyExc_TypeError, "Error parsing agent dictionary in lebiology.add_agent");
//      Py_INCREF(PyExc_TypeError);
//      return NULL;
//   }

//   // Check for correctly parsed agent argument
//   if (!pPositionList) {
//      PyErr_SetString(PyExc_TypeError, "Error parsing position list in lebiology.add_agent");
//      Py_INCREF(PyExc_TypeError);
//      return NULL;
//   }

//   // Check if given FGroup exists
//   PyObject *pVarNames = PyDict_GetItemString(pFGVarNames, fg);
//   if (!pVarNames) {
//      PyErr_SetString(PyExc_KeyError, "FGroup not found in lebiology.add_agent");
//      Py_INCREF(PyExc_KeyError);
//      return NULL;
//   }

//   // Convert the agent dict
//   int *n_vars = (int *)malloc(sizeof(int));
//   *n_vars = PyList_Size(pVarNames);
//   double *vars = (double *)malloc(*n_vars * sizeof(double));
//   int i;
//   for (i=0; i<*n_vars; i++) {
//     pAgentVar = PyDict_GetItem(pAgentDict, PyList_GET_ITEM(pVarNames, i));
//     if (pAgentVar) {
//       vars[i] = PyFloat_AsDouble(pAgentVar);
//     } else {
//       vars[i] = 0.0;
//     }
//   }

//   //Convert the position list
//   int *n_pos = (int *)malloc(sizeof(int));
//   *n_pos = PyList_Size(pPositionList);
//   assert(*n_pos == lebiology_dim);
//   double *pos = (double *)malloc(lebiology_dim * sizeof(double));
//   for (i=0; i<*n_pos; i++) {
//     pos[i] = PyFloat_AsDouble( PyList_GET_ITEM(pPositionList, i) );
//   }

//   //Convert food dict
//   PyObject *pFoodNames = PyDict_GetItemString(pFGFoodNames, fg);
//   if (pFoodNames) {
//     PyObject *pFoodKeys = PyDict_Keys(pFoodNames);
//     PyObject *pFoodName = PyList_GetItem(pFoodKeys, 0);
//     PyObject *pVarietyNames = PyDict_GetItem(pFoodNames, pFoodName);
//     int n_fvariety = PyList_Size(pVarietyNames);

//     char *food = PyString_AsString(pFoodName);
//     double *frequest = (double *)malloc(n_fvariety * sizeof(double));
//     double *fthreshold = (double *)malloc(n_fvariety * sizeof(double));
//     convert_food_variety(pAgentDict, fg, food, n_fvariety, frequest, fthreshold);

//     // Pass the new agent on to the Fortran with food requests
//     fl_add_agent_food_c(vars, n_vars, pos, &lebiology_dim, frequest, fthreshold, &n_fvariety);

//     free(frequest);
//     free(fthreshold);
//     Py_DECREF(pFoodKeys);
//   } else {
//     // Pass the new agent on to the Fortran
//     fl_add_agent_c(vars, n_vars, pos, &lebiology_dim);
//   }

//   free(vars);
//   free(n_vars);
//   free(pos);
//   free(n_pos);

//   Py_INCREF(Py_None);
//   return Py_None;
// #endif
// }
