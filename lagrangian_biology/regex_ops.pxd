from libcpp.string cimport string
from libcpp.vector cimport vector

cdef extern from "regex_ops.h" nogil:
    vector[string] * findImportedModules(const string &)
