#include "regex_ops.h"
#include <iostream>
#include <regex>
#include <sstream>
using namespace std;

regex importExp ("(?:\\nc?import\\s+(?:([a-zA-Z0-9_\\.]+)(?:\\s+as\\s+[a-zA-Z0-9_\\.]+)?,?)+)|(?:from\\s+([a-zA-Z0-9_\\.]+)\\s+c?import\\s+\\*)");
// Need special handling for other types of imports
// from mod import submod, asf
//|(?:from\\s+([a-zA-Z0-9_\\.]+)\\s+[^c]import\\s+\\(?(?:([a-zA-Z0-9_\\.]+)(?:\\s+as\\s+[a-zA-Z0-9_\\.]+)?)+\\)?)");

string& trim_left_in_place(string& str) {
    size_t i = 0;
    while(i < str.size() && isspace(str[i])) { ++i; };
    return str.erase(0, i);
}

string& trim_right_in_place(string& str) {
    size_t i = str.size();
    while(i > 0 && isspace(str[i - 1])) { --i; };
    return str.erase(i, str.size());
}

string& trim_in_place(string& str) {
    return trim_left_in_place(trim_right_in_place(str));
}

string trim_right(string str) {
    return trim_right_in_place(str);
}

string trim_left(string str) {
    return trim_left_in_place(str);
}

string trim(string str) {
    return trim_left_in_place(trim_right_in_place(str));
}

vector<string> &split(const string &s, char delim, vector<string> &elems) {
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

vector<string> split(const string &s, char delim) {
    vector<string> elems;
    split(s, delim, elems);
    return elems;
}

vector<string> * findImportedModules(const string & code) {
    string searchCode(code);
    vector<string> * foundModules = new vector<string>();
    smatch matchResults;

    while(regex_search(searchCode, matchResults, importExp)) {
        int first = true;

        for(auto mod : matchResults) {
            // First result is a full string which we are not interested in
            if (first) {
                first = false;
                continue;
            }
            string resultMod = trim(mod.str());
            if(resultMod != "" && resultMod != "lebiology") {
                vector<string> modParts = split(resultMod, '.');
                foundModules->push_back(modParts.back());
            }
        }
        searchCode = matchResults.suffix().str();
    }

    return foundModules;
}

