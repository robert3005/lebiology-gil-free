from libc.stdio cimport printf, snprintf
from libc.string cimport strcmp, strlen
from libcpp.unordered_map cimport unordered_map
from libcpp.vector cimport vector
from libcpp.string cimport string
from libcpp.pair cimport pair
from libcpp cimport bool
from cython.operator cimport dereference as deref
cimport openmp
cimport functional_group.functional_group as kernelFn

from path_ops cimport *
from LEbiology_Python cimport *
from regex_ops cimport *

ctypedef string * strPtr
ctypedef vector[strPtr] * vecStr
ctypedef unordered_map[string, double] * mapStr
ctypedef unordered_map[string, vecStr] * mapVec

# callback functions
cdef double stage_id(char * fg, char * stage) nogil:
    global pFGStageID
    cdef string sFg = string(fg)
    cdef string sStage = string(stage)
    return pFGStageID.at(sFg).at(sStage)


cdef void add_agent(char * fg, double * vars, double * pos) nogil:
    pass


cdef void dropout_agent() nogil:
    global dropoutList
    cdef int tid = openmp.omp_get_thread_num()
    deref(dropoutList)[tid] = True


cdef int lebiology_dim
cdef string path
cdef string subDirName
cdef unordered_map[string, vecStr] * pFGVarNames
cdef unordered_map[string, vecStr] * pFGEnvNames
cdef unordered_map[string, mapStr] * pFGStageID
cdef unordered_map[string, mapVec] * pFGFoodNames
cdef vector[bool] * dropoutList

# /**** Initialisation function for the module 'lebiology' ****/
cdef public void lebiology_setup(char * pathIn, int dim) nogil:
    global pFGVarNames, pFGEnvNames, pFGStageID, pFGFoodNames, lebiology_dim, path, subDirName, dropoutList
    pFGVarNames = new unordered_map[string, vecStr]()
    pFGEnvNames = new unordered_map[string, vecStr]()
    pFGStageID = new unordered_map[string, mapStr]()
    pFGFoodNames = new unordered_map[string, mapVec]()
    dropoutList = new vector[bool]()

    cdef int num_threads = openmp.omp_get_max_threads()
    cdef int i
    for i in range(num_threads):
        dropoutList.push_back(False)

    lebiology_dim = dim
    path = parentPath(absolutePath(string(pathIn)))
    subDirName = string(<char *>"functional_group")

#
# Meta-model utilities
#

# /* Add a variable name to the array under pFGVarNames['fg'] */
cdef public void lebiology_add_fg_varname_c(char *fg, char *var, int *stat) nogil:
    global pFGVarNames
    cdef string sFg = string(fg)
    cdef string * sVar = new string(var)
    if (pFGVarNames.find(sFg) == pFGVarNames.end()):
        pFGVarNames.insert(pair[string, vecStr](sFg, new vector[strPtr]()))

    pFGVarNames.at(sFg).push_back(sVar)
    stat[0] = 0


# /* Add an environment name to the array under pFGEnvNames['fg'] */
cdef public void lebiology_add_fg_envname_c(char *fg, char *env, int *stat) nogil:
    global pFGEnvNames
    cdef string sFg = string(fg)
    cdef string * sEnv = new string(env)
    if (pFGEnvNames.find(sFg) == pFGEnvNames.end()):
        pFGEnvNames.insert(pair[string, vecStr](sFg, new vector[strPtr]()))

    pFGEnvNames.at(sFg).push_back(sEnv)
    stat[0] = 0


# /* Add a food variety name to the array under pFGFoodNames['fg']['food'] */
cdef public void lebiology_add_fg_foodname_c(char *fg, char *food, char *variety, int *stat) nogil:
    global pFGFoodNames
    cdef string sFg = string(fg)
    cdef string sFood = string(food)
    cdef string * sVariety = new string(variety)
    if (pFGFoodNames.find(sFg) == pFGFoodNames.end()):
        pFGFoodNames.insert(pair[string, mapVec](sFg, new unordered_map[string, vecStr]()))

    if (pFGFoodNames.at(sFg).find(sFood) == pFGFoodNames.at(sFg).end()):
        pFGFoodNames.at(sFg).insert(pair[string, vecStr](sFood, new vector[strPtr]()))

    pFGFoodNames.at(sFg).at(sFood).push_back(sVariety)
    stat[0] = 0


# /* Add a stage ID to pFGStageID['fg'] */
cdef public void lebiology_add_fg_stage_id_c(char *fg, char *stage, double *id, int *stat) nogil:
    cdef string sFg = string(fg)
    cdef string sStage = string(stage)
    if(pFGStageID.find(sFg) == pFGStageID.end()):
        pFGStageID.insert(pair[string, mapStr](sFg, new unordered_map[string, double]()))

    deref(pFGStageID.at(sFg))[sStage] = deref(id)
    stat[0] = 0


################################
# Embedded Python interfaces
################################

cdef public void lebiology_create_compile_dir_c(int *stat) nogil:
    global path
    cdef string rootInit = absolutePath(string(<char *>"__init__.py"))
    cdef string outputDir = absolutePath(subDirName)
    cdef vector[string] pathParts

    pathParts.push_back(path)
    pathParts.push_back(string(<char *>"../lagrangian_biology/functional_group/functional_group.pxd"))
    cdef string topPxdFile = composePath(pathParts)

    pathParts.clear()
    pathParts.push_back(outputDir)
    pathParts.push_back(string(<char *>"functional_group.pxd"))
    cdef string destPxdFile = composePath(pathParts)

    remove(outputDir)
    remove(rootInit)

    cdef bool success
    success = create(outputDir)

    if not success:
        stat[0] = -1
        return

    copy(topPxdFile, destPxdFile)

    cdef vector[string] content
    content.push_back(string(<char *>"\n"))
    writeFile(rootInit, content)

    stat[0] = 0


cdef public void lebiology_create_compile_dir_fgroup_c(char *fg, int *stat) nogil:
    cdef vector[string] pathParts
    pathParts.push_back(subDirName)
    pathParts.push_back(string(fg))

    cdef string outputDir = composePath(pathParts)
    create(outputDir)


#  compile_function runs the specified Python function 'def val(...)'
# from the schema and stores the local namespace under pFGNamespace['fg'].
# The compiled function object for can be retrieved from the namespace
# and applied over agents with different arguments.
cdef void replaceNameWithIndex(string & src, string & fromStr, int to) nogil:
    cdef int idx
    cdef int lastIdx
    cdef string lookupStr
    cdef char itos[3]
    snprintf(itos, 3, "%d", to)

    lookupStr = string(<char *>"'")
    lookupStr += fromStr
    lookupStr += string(<char *>"'")
    idx = src.find(lookupStr)
    while idx != -1:
        src.replace(idx, lookupStr.size(), string(itos))
        lastIdx = idx
        idx = src.find(lookupStr, lastIdx + strlen(itos))


cdef void performSubstitutions(char *fg, string & code) nogil:
    cdef vecStr varSubs = pFGVarNames.at(string(fg))
    cdef vecStr envSubs = pFGEnvNames.at(string(fg))

    for i in range(varSubs.size()):
        replaceNameWithIndex(code, deref(varSubs.at(i)), i)

    for i in range(envSubs.size()):
        replaceNameWithIndex(code, deref(envSubs.at(i)), i)


cdef public void lebiology_compile_function_c(char *fg, char * key, char *func, int *stat) nogil:
    cdef vector[string] pathParts
    pathParts.push_back(subDirName)
    pathParts.push_back(string(fg))

    cdef string outputDir = composePath(pathParts)
    pathParts.clear()
    pathParts.push_back(outputDir)
    pathParts.push_back(string(key) + string(<char *>".pyx"))
    cdef string destFile = composePath(pathParts)

    cdef string codeStr = string(func)
    cdef vector[string] * modules = findImportedModules(codeStr)
    performSubstitutions(fg, codeStr)


    cdef string absPath
    cdef string fileName
    cdef string pxdFile
    cdef string absPxdPath
    cdef string modContents
    cdef string destPath
    for i in range(modules.size()):
        pxdFile = modules.at(i) + <char *>".pxd"
        absPxdPath = absolutePath(pxdFile)
        fileName = modules.at(i) + <char *>".py"
        absPath = absolutePath(fileName)
        modContents = readFile(absPath)
        performSubstitutions(fg, modContents)
        pathParts.pop_back()
        pathParts.push_back(fileName + <char *>"x")
        destPath = composePath(pathParts)
        writeFile(destPath, modContents)

        pathParts.pop_back()
        pathParts.push_back(pxdFile)
        destPath = absolutePath(composePath(pathParts))
        remove(destPath)
        copy(absPxdPath, destPath)


    cdef string funDef
    cdef string cFunDef
    if strcmp(key, <char *>"Init") == 0:
        funDef = string(<char *>"def val(agent):")
        cFunDef = string(<char *>"cdef double * val(double * agent) nogil:")
    else:
        funDef = string(<char *>"def val(agent, env, dt):")
        cFunDef = string(<char *>"cdef double * val(double * agent, double * env, double dt) nogil:")

    cdef int idx = codeStr.find(funDef)
    if idx != -1:
        codeStr.replace(idx, funDef.size(), cFunDef)

    cdef vector[string] content
    content.push_back(codeStr)
    content.push_back(string(<char *>"\n\n"))
    writeFile(destFile, content)

    stat[0] = 0


cdef public void lebiology_compile_ext() nogil:
    cdef vector[string] pathParts

    pathParts.push_back(path)
    pathParts.push_back(string(<char *>"../lagrangian_biology/setup_ext.py"))
    cdef string sourceSetup = composePath(pathParts)
    cdef string pathDist = absolutePath(string(<char *>"setup_ext.py"))

    remove(pathDist)
    copy(sourceSetup, pathDist)
    cdef string buildExt = string(<char *>"python ")
    buildExt += pathDist
    buildExt += string(<char *>" build_ext --inplace")
    cdef int result = system(buildExt.c_str())
    if result != 0:
        pass # handle error


cdef public void lebiology_agent_init_c(char *fg, double vars[], int n_vars, int *stat) nogil:
    kernelFn.initAgent(fg, vars)
    stat[0] = 0


# stub for importing functions
cdef public void lebiology_agent_update_c(char *fg, char *key, char *food, double vars[], int n_vars, double envvals[], int n_envvals,
                              double fvariety[], double frequest[], double fthreshold[], double fingest[], int n_fvariety,
                              double *dt, int *dropout, int *stat) nogil:
    if n_fvariety > 0:
        pass
        # need to find smart way to implement this
        # add_food_variety(vars, envvals, fg, food, n_fvariety, fvariety, fingest)

    kernelFn.updateAgent(fg, key, vars, envvals, deref(dt))
    stat[0] = 0

    cdef int tid = openmp.omp_get_thread_num()
    if deref(dropoutList)[tid]:
        dropout[0] = True
        deref(dropoutList)[tid] = False
    else:
        dropout[0] = False

    if n_fvariety > 0:
        pass
        # need to find smart way to implement this
        # convert_food_variety(vars, fg, food, n_fvariety, frequest, fthreshold)
