import os
import functools
from distutils.core import setup
from Cython.Build import cythonize
from Cython.Distutils import build_ext, Extension
from Cython.Compiler import Options

# generate annotations
Options.annotate = True
# Don't raise python exceptions on division by 0
# Necessary for gil free execution
# Per module settings are ignored - set globally
Options.directive_defaults["cdivision"] = True
Options.directive_defaults["unraisable_tracebacks"] = False

extensions = []
groups = []

initTmpl = """
    if strcmp(fg, '{0}') == 0:
        {0}.init(vars)\n
"""

updateTmpl = """
    if strcmp(fg, '{0}') == 0:
        {0}.update(key, agent, env, dt)\n
"""

updateModTmpl = """
    if strcmp(key, '{0}') == 0:
        {0}(agent, env, dt)
"""

initModTmpl = """
    {0}(vars)\n
"""

importTmpl = "cimport functional_group.{0}.{0} as {0}\n"
valImportTmpl = "from {1}.{0} cimport val as {0}\n"

def generatePerFileHeaders(path, file):
    file = os.path.join(path, file.replace("pyx", "pxd"))
    if "Init" in file:
        with open(file, "w") as f:
            f.write("cdef double * val(double *) nogil\n")

    if "_Update" in file:
        with open(file, "w") as f:
            f.write("cdef double * val(double *, double *, double) nogil\n")


def generateFgModule(module, files):
    path = module.replace(".", "/")
    # necessary for python module system
    with open(path + "/__init__.py", "w") as f:
        f.write("\n")

    newPxd = module.split(".")
    importName = newPxd[-1]

    with open(path + "/" + importName + ".pxd", "w") as f:
        f.write("cdef void init(double * vars) nogil\n")
        f.write("cdef void update(char * key, double * agent, double * env, double dt) nogil\n")

    imports = ""
    initCode = "cdef void init(double * vars) nogil:"
    updateCode = "cdef void update(char * key, double * agent, double * env, double dt) nogil:"
    for file in files:
        impMod = os.path.splitext(file)[0]
        if "_Update" in file or "Init" in file:
            imports += valImportTmpl.format(impMod, module)

        if "Init" in file:
            initCode += initModTmpl.format(impMod)

        if "_Update" in file:
            updateCode += updateModTmpl.format(impMod)


    with open(path + "/" + importName + ".pyx", "w") as f:
        f.write("from libc.string cimport strcmp\n")
        f.write(imports)
        f.write("\n")
        f.write(initCode)
        f.write("\n")
        f.write(updateCode)
        f.write("\n")


def generateTopLevelModule(modules):
    with open("functional_group/__init__.py", "w") as f:
        f.write("\n")

    modules = map(lambda name: ".".join(name.split(".")[1:]), filter(lambda name: "." in name, modules))
    initFn = "cdef void initAgent(char * fg, double * vars) nogil:"
    updateFn = "cdef void updateAgent(char * fg, char * key, double * agent, double * env, double dt) nogil:"
    imp = ""
    for module in modules:
        imp += importTmpl.format(module)
        updateFn += updateTmpl.format(module)
        initFn += initTmpl.format(module)

    with open("functional_group/functional_group.pyx", "w") as f:
        f.write("from libc.string cimport strcmp\n")
        f.write(imp)
        f.write("\n")
        f.write(initFn)
        f.write("\n")
        f.write(updateFn)
        f.write("\n")


for dirName, dirs, files in os.walk("functional_group"):
    packageName = dirName.replace("/", ".")
    groups.append(packageName)
    compileFiles = filter(lambda f: os.path.splitext(f)[1] == ".pyx", files)
    map(functools.partial(generatePerFileHeaders, dirName), compileFiles)
    if "." in packageName:
        generateFgModule(packageName, compileFiles)

generateTopLevelModule(groups)

packages = []

for dirName, dirs, files in os.walk("functional_group"):
    compileFiles = map(lambda f: dirName + "/" + f,
        filter(lambda f: os.path.splitext(f)[1] == ".pyx", files))
    for file in compileFiles:
        packageName = os.path.splitext(file)[0].replace("/", ".")
        extensions.append(Extension(packageName, [file]))
        packages.append(packageName)

print packages

for e in extensions:
    e.include_dirs = ['.', os.path.abspath(e.name.replace(".", "/"))]
    e.language = "c++"
    e.extra_compile_args = ["-fopenmp", "-std=c++11", "-I/home/robert3005/fluidity/lagrangian_biology/lagrangian_biology"]
    e.extra_link_args = ["-fopenmp", "-lstdc++"]


setup(
    name = "FnGroup",
    packages = packages,
    cmdclass = {"build_ext": build_ext},
    ext_modules = cythonize(extensions)
)
