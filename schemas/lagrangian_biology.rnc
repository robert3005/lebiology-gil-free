lagrangian_biology =
   (
      ## Lagrangian particle biology from the VEW project.
      element lagrangian_ensemble_biology {
         ## Seed used for the random number generator. 
         element random_seed{
            integer
         },
         ## Diagnostic field for calculating the required number of sub-timesteps for automated Random Walk subcycling.
         element scalar_field {
            attribute name { 'RandomWalkSubcycling' },
            element scale_factor { 
               real
            },
            element diagnostic {
               internal_algorithm,
               diagnostic_scalar_field_lebiology
            }
         }?,
         (
            ## A Functional Group (FG) is an abstraction to define the characteristic behaviour of one or more species of plankton agents. 
            ## The Functional Group defines the internal variables an agent will store and the environment fields an agent needs to sample. 
            ## The concrete behaviour of an agent, however, is defined by the stage the agent is in. Stage changes are only permitted within Functional Groups. 
            element functional_group {
               attribute name { xsd:string },
               (
                  ## The variables of an agent define its individual internal state. 
                  ## Several types of variables are available: 
                  ## state_variable defines an arbitrary internal property; 
                  ## chemical_variable defines an internal pool of chemicals that can be exchanged with the environment. 
                  element variables {

                     ## An agent's stage determines the update function to apply at each timestep.
                     element state_variable {
                        attribute name { 'Stage' },
                        element include_in_io {
                           empty
                        }?
                     },
                     ## Dynamic size of the ensemble the agent represents. 
                     ## All internal quantities that are exchanged with the environment get scaled by this factor.
                     element state_variable {
                        attribute name { 'Size' },
                        ## Biology agent field
                        element scalar_field {
                           attribute name { 'Ensemble' },
                           element diagnostic {
                              internal_algorithm,
                              diagnostic_scalar_field_lebiology
                           },
			   element integrate_along_path {
                              empty
                           }?,
                           ## Add diagnostic fields that represent the particulate concetration of individual stage groups.
                           element stage_diagnostic {
                              empty
                           }
                        },
                        element include_in_io {
                           empty
                        }?
                     },
                     (
                        state_variable |
                        chemical_variable
                     )*
                  }
               ),
               element initial_state {
                  ## The number of detectors prescribed by the python function.
                  element number_of_agents {
                     integer
                  },                     
                  ## Python function prescribing dimensional vector input. Functions should be of the form:
                  ##
                  ##  def val(t):
                  ##     # Function code
                  ##     return # Return value
                  ##
                  ## The return value must have length number_of_detectors.
                  ##
                  ## *** IMPORTANT NOTE ***
                  ##
                  ## The t argument is for future use only - currently detector locations are only set at simulation start.
                  element position {
                     python_code
                  },
                  (
                     ## Python function to set the initial state of the agent. 
                     ## A dictionary mapping variable name to value gets passed to the function, where the stage ID is already set,
                     ## which needs to be returned with the appropriate initial variable values, eg.:
                     ##
                     ##  def val(biovars):
                     ##     biovars['VarName'] = 1.0
                     ##     return biovars
                     element biology {
                        python_code
                     }
                  )
               },
               agent_movement,
               (
                  ## This option holds a list of fields that define the ambient environment for agents of this Functional Group. 
                  ## The local values of fields listed here will be presented in a dictionary 'environment' to the agent update function.
                  element environment {
                     ## Environment field whose ambient value will be presented to the update code in the 'environment' dictionary.
                     element field {
                        attribute name { xsd:string },
                        (
                           element integrate_along_path {
                              empty
                           }|
                           element old_position {
                              empty
                           }
                        )?
                     }*
                  }
               ),
               ##
               element stages {
                  ## Detectors with their locations specified via a python function or from a file. Allows detector arrays to be added.
                  element stage {
                     comment,
                     attribute name { xsd:string },
                     (
                        ## Biology model for this type of agent.
                        element biology {
                           (
                              ## Python code specifying the biology model.
                              element python {
                                 python_code
                              }|
                              ## Update kernel function to apply to the agent. 
                              ## This is an external python function wrapped in a module somewhere on the PYTHONPATH.
                              element update_kernel {
                                 attribute name { xsd:string },
                                 attribute module { xsd:string },
                                 attribute parameters { xsd:string },
                                 ## Expose the "persistent" dictionary as an extra argument to the kernel function.
                                 element expose_persistent {
                                    empty
                                 }?
                              }
                           )
                        }|
                        ## Do not calculate biology
                        ## This option is generally only useful for testing. 
                        element disable_biology {
                           empty
                        }
                     ),
                     ## Particle Management rules
                     element particle_management {
                        element period_in_timesteps {
                           integer
                        },
                        element scalar_field {
                           attribute name { "AgentsMin" },
                           element prescribed {
                              element mesh {
                                 attribute name { "LEBiologyMesh" }
                              },
                              prescribed_scalar_field_no_adapt
                           }
                        }?,
                        element scalar_field {
                           attribute name { "AgentsMax" },
                           element prescribed {
                              element mesh {
                                 attribute name { "LEBiologyMesh" }
                              },
                              prescribed_scalar_field_no_adapt
                           }
                        }?,
                        ## Local maximum number of agents in this list.
                        ## Arbitrary agent pairs will be merged to enforce this limit.
                        element local_maximum {
                           integer
                        }?
                     }?,
                     (
                     	element disable_io {
                        	empty
                     	}|
                     	element io_period {
                        	real
                     	}
                     )
                  }*
               },
               food_set,
               ## Number of agents
               element scalar_field {
                  attribute name { 'Agents' },
                  element diagnostic {
                     internal_algorithm,
                     diagnostic_scalar_field_lebiology
                  }
               }
            }|
            element external_fgroup {
               attribute name { xsd:string },
               ## External FGroups need to create fields associated with chemical variables...
               element variables {
                  chemical_variable*
               },
               element scalar_field {
                  attribute name { "Concentration" },
                  (
                     element prescribed {
                        element mesh {
                           attribute name { "LEBiologyMesh" }
                        },
                        prescribed_scalar_field_no_adapt,
                        adaptivity_options_scalar_field,
                        element galerkin_projection {
                           element discontinuous {
                             empty
                           }
                        },
                        recalculation_options?
                     }|
                     element diagnostic {
                        (
                           scalar_galerkin_projection_algorithm
                        ),
                        diagnostic_scalar_field_lebiology
                     }
                  )
               },
               food_set,
               (
                  ## Biology model for this functional group
                  element biology {
                     (
                        ## Python code specifying the biology model
                        element python {
                           python_code
                        },
                        (
                           element field_alias {
                              attribute name { xsd:string }
                           }
                        )*
                     )
                  }|
                  ## Do not calculate biology
                  element disable_biology {
                     empty
                  }
               )
            }
         )*,
         (
            ## Python state invocation after agent update used for coupling models. 
            ## For performance reasons only the specified fields are accessible.
            element python_pre_process {
               python_code,
               (
                  element field_alias {
                     attribute name { xsd:string }
                  }
               )*
            }?
         ),
         (
            ## Python state invocation before agent update used for coupling models. 
            ## For performance reasons only the specified fields are accessible.
            element python_post_process {
               python_code,
               (
                  element field_alias {
                     attribute name { xsd:string }
                  }
               )*
            }?
         ),
         (
            ## Python state invocation executed after deriving ingestion requests.
	    ## This is a hack to emulate the VEW algorithm for agent-agent predation.
            ## For performance reasons only the specified fields are accessible.
            element python_ingest_hook {
               python_code,
               (
                  element field_alias {
                     attribute name { xsd:string }
                  }
               )*
            }?
         )
      }
   )

state_variable = 
   (
      ## Internal state variable of a biology agent that defines an arbitrary internal property.
      element state_variable {
         attribute name { xsd:string },
         ## Expose this variable to the Python Random Walk interface, as well as the biology update.
         element include_in_motion {
            empty
         }?,
         ## Depth of history buffer
         element history {
            integer
         }?,
         element include_in_io {
            empty
         }?
      }
   )

chemical_variable = 
   (
      ## State variable representing the internal chemical pool of an agent. 
      ## Chemicals can be exchanged with the environment via the 'release' and 'uptake' options. 
      ## This requires a dissolved chemical field from the main material_phase to be bound to this agent variable. 
      ## The associated source and absorption fields will then get set from the accumulated particulate quantities.
      element chemical_variable {
         attribute name { xsd:string },
         ## Eulerian diagnostic field representing the particulate amount of this chemical. 
         ## This field must be on a discontinuous mesh
         element scalar_field {
            attribute name { 'Particulate' },
            element diagnostic {
               internal_algorithm,
               diagnostic_scalar_field_lebiology
            },
            element integrate_along_path {
               empty
            }?,
            ## Add diagnostic fields that represent the particulate concetration of individual stage groups.
            element stage_diagnostic {
               empty
            }?
         }?,
         ## Ingested field on a discontinuous mesh
         element scalar_field {
            attribute name { "Ingested" },
            element diagnostic {
               internal_algorithm,
               diagnostic_scalar_field_lebiology
            },
            ## Add diagnostic fields that represent the particulate concetration of individual stage groups.
            element stage_diagnostic {
               empty
            }?,
            element include_in_io {
               empty
            }?
         }?,
         ## Option to define chemical uptake from the environment. 
         ## This will create internal fields to represent the accumulated requested quantity 
         ## and the depletion factor, which scales the amount actually ingested by the agent 
         ## according to availability in the environment. 
         ## The scaled request quantity will then get added to the according absorption field of the dissolved chemical.
         element uptake {
            ## Name of the source field representing the dissolved chemical in the environment
            element source_field {
               attribute name { xsd:string }
            },
            ## Request field on a discontinuous mesh
            element scalar_field {
               attribute name { "Request" },
               element diagnostic {
                  internal_algorithm,
                  diagnostic_scalar_field_lebiology
               }
            },
            ## Depletion factor
            element scalar_field {
               attribute name { "Depletion" },
               element diagnostic {
                  internal_algorithm,
                  diagnostic_scalar_field_lebiology
               }
            },
            element integrate_along_path {
               empty
            }?,
            element include_in_io {
               empty
            }?
         }?,
         ## Option to define chemical release into the environment. 
         ## This will create internal fields to represent the accumulated release quantity.
         ## The scaled release quantity will then get added to the according source field of the dissolved chemical.
         element release {
            ## Name of the target field representing the dissolved chemical in the environment
            element target_field {
               attribute name { xsd:string }
            },
            ## Release field on a discontinuous mesh
            element scalar_field {
               attribute name { "Release" },
               element diagnostic {
                  internal_algorithm,
                  diagnostic_scalar_field_lebiology
               }
            },
            ## Optional field to accumulate total chemical release over time.
            element scalar_field {
               attribute name { "ReleaseTotal" },
               element diagnostic {
                  internal_algorithm,
                  diagnostic_scalar_field_lebiology
               }
            }?,
            element integrate_along_path {
               empty
            }?,
            element include_in_io {
               empty
            }?
         }?,
         element include_in_io {
            empty
         }?
      }
   )

food_set = 
   (
      element food_set {
         attribute name { xsd:string },
         attribute functional_group { xsd:string },
         ## Concentration field on a discontinuous mesh mesh
         element scalar_field {
            attribute name { "Concentration" },
            element diagnostic {
               internal_algorithm,
               element mesh {
                  attribute name { "LEBiologyMesh" }
               },
               diagnostic_output_options,
               diagnostic_scalar_stat_options,
               adaptivity_options_scalar_field,
               element do_not_recalculate {
                  empty
               },
               element galerkin_projection {
                  element discontinuous {
                    empty
                  }
               }
            }
         },
         element scalar_field {
            attribute name { "Request" },
            element diagnostic {
               (
                  internal_algorithm
                  | scalar_galerkin_projection_algorithm
               ),
               diagnostic_scalar_field_lebiology
            },
            element stage_diagnostic {
               empty
            }?,
            element include_in_io {
               empty
            }?
         },
         ## Depletion factor
         element scalar_field {
            attribute name { "Depletion" },
            element diagnostic {
               internal_algorithm,
               diagnostic_scalar_field_lebiology
            }
         },
         element scalar_field {
            attribute name { "IngestedCells" },
            element diagnostic {
               internal_algorithm,
               diagnostic_scalar_field_lebiology
            },
            element stage_diagnostic {
               empty
            }?,
            element include_in_io {
               empty
            }?
         },
         element integrate_along_path {
            empty
         }?,
         ## Individual food type for this set
         element food_type {
            attribute name { xsd:string }
         }*
      }?
   )

agent_movement = 
   (
      ## Movement options
      element movement {
         ## Number of explicit subdivisions of the timestep.
         element subcycles {
            integer
         },
         ## Tolerance for deciding if detector is in a given
         ## element. Recommended value 1.0e-10
         element search_tolerance {
            real
         },
         (
            element reflect_on_boundary{
               empty
            }|
            element turn_static_on_boundary{
               empty
            }
         ),
         (
            element parametric_guided_search{
               empty
            }|
            element pure_guided_search{
               empty
            }|
            element geometric_tracking{
               element periodic_mesh {
                  mesh_choice
               }?
            }
         ),
         (
            ## Use explicit Forward Euler method with
            ## guided search particle tracking
            element forward_euler_guided_search {
               empty
            }|
            ## Use fourth order Runge-Kutta method with
            ## guided search particle tracking
            element rk4_guided_search {
               empty
            }|
            ## Use explicit runge kutta method with
            ## guided search particle tracking
            element explicit_runge_kutta_guided_search {
               ## Number of RK stages
               ## For the RK4 method, it should be 4.
               element n_stages {
                  integer
               },
               ## ERK stage array. This is an array
               ## containing the lower-triangular
               ## part of the Butcher weight matrix
               ## A that explains how to compute the
               ## RK stages.  See
               ## http://en.wikipedia.org/wiki/Runge–Kutta_methods#Explicit_Runge.E2.80.93Kutta_methods
               ## for notation.  The array is stored
               ## in the following order:
               ## [a_{21},a_{31},a_{32},...,a_{s1},a_{s2},a_{s(s-1)}]
               ## and so the array has size s(s-1)/2
               ## where s is the number of stages.
               ## For the RK4 method, it should be
               ## [0.5,0,0.5,0,0,1]
               element stage_weights {
                  real_vector
               },
               ## ERK timestep weights. This is the
               ## b vector that explains how to
               ## compute the timestep from the RK
               ## stages.  See
               ## http://en.wikipedia.org/wiki/Runge–Kutta_methods#Explicit_Runge.E2.80.93Kutta_methods
               ## for notation.  It should have size
               ## s where s is the number of stages.
               ## For the RK4 method, it should be
               ## [1/6,1/3,1/3,1/6]
               element timestep_weights {
                  real_vector
               }
            }
         )?,
         (
            ##
            element random_walk {
               attribute name { xsd:string },
               ## Python function prescribing additional agent displacement due to turbulence. 
               ## The return value will be added to the lagrangian advection at each sub-timestep. 
               ## Fields should be pulled from 'state' before the val(ele, local_coord) function is evaluated.
               ## Functions should be of the form:
               ## 
               ##  field = state.scalar_fields['Field_name']
               ##
               ##  def val(ele, local_coord):
               ##     # x = field.eval_field(ele, local_coord)
               ##     return # Return some f(x)
               element python{
                  python_code
               }
            }|
            ## Hardcoded Random Walk scheme for non-homogeneous diffusivity fields.
            ## Described in Visser, 1997 and Ross and Sharples, 2004.
            ## Requires name of diffusivity field and according gradient field.
            element random_walk {
               attribute name {"Diffusive"},
               attribute diffusivity_field { xsd:string },
               attribute diffusivity_gradient { xsd:string },
               element auto_subcycle {
                  empty
               }?
            }|
            ## Hardcoded Naive Random Walk scheme for verifying the RNG
            element random_walk {
               attribute name {"Naive"},
               attribute diffusivity_field { xsd:string }
            }
         )*
      }
   )

diagnostic_scalar_field_lebiology = 
   (
      element mesh {
         attribute name { "LEBiologyMesh" }
      },
      diagnostic_output_options,
      diagnostic_scalar_stat_options,
      adaptivity_options_scalar_field,
      element do_not_recalculate {
         empty
      },
      element galerkin_projection {
         element discontinuous {
           empty
         }
      }
   )

hyperlight = 
   (
      ## Hyperlight solar irradiance model, which 
      ## computes a fast approximation to the Radiative Transfer Equation.
      ## NOTE: This module requires fluidity to be compiled with the 
      ## --enable-hyperlight flag.
      element hyperlight {
         ## Hydrolight always computes the scalar irradiance 
         ## for 36 regular wavebands of width 10nm in PAR. 
         ## Particular wavebands or total PAR irradiance need 
         ## to be derived from these fixed bands.
         element scalar_field {
            attribute rank { "0" },
            attribute name { "IrradianceTemplate" },
            element prescribed {
               velocity_mesh_choice,
               prescribed_scalar_field
            }
         },
         element scalar_field {
            attribute rank { "0" },
            attribute name { "IrradiancePAR" },
            element diagnostic {
               velocity_mesh_choice,
               scalar_python_diagnostic_algorithm,
               diagnostic_scalar_field_no_adapt
            }
         }?,
         ## F, the ratio for correlated CDOM
         element CDOM {
            real
         },
         ## BF_chl, the backscatter fraction for chlorophyll-bearing particles
         element BF_chl {
            real
         },
         ## Parameter for GC sky irradiance model - 
         ## if provided this overwrites the values from the forcing routines.
         element CloudCover {
            real
         }?,
         ## Parameter for GC sky irradiance model - 
         ## if provided this overwrites the values from the forcing routines.
         element WindSpeed {
            real
         }?,
         ## Optional performance parameter that determines the percentage of 
         ## surface irradiance at which the model stops computing.
         ## Default is 0.01 = 1% (standard definition of euphotic depth)
         element EuphoticRatio {
            real
         }?
      }
   )
