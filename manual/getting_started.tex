\chapter{Getting started}\label{chap:gettingstarted}

\section{Introduction} This first chapter gives a brief guide to setting-up,
running and visualising simulations. It is assumed that you have access to a
system running Linux or UNIX with all the \fluidity\ supporting libraries
installed. If not, you may wish to reference appendix \ref{chap:external} which
contains detailed information for your systems administrator to help setting up
a Linux workstation. 

\section{Obtaining \fluidity}
\label{sec:obtaining_fluidity}

\subsection{Overview}
\label{sec:obtaining_fluidity_overview}

\fluidity\ is available both as precompiled binaries for Ubuntu Linux version
12.04 (Precise) and newer, and as source code available via bzr (bazaar) or as
gzipped tarballs. Which method you use to obtain \fluidity\ depends primarily on
whether you use Ubuntu Linux and whether you wish to modify the \fluidity\
source code. 

\fluidity\ generally attempts to support any given release of Ubuntu for 18
months from the point of release.

Users who run Ubuntu Linux and who have no need to change the source code will
probably wish to use binary packaged \fluidity. Developers and users on
non-Ubuntu platforms will probably wish to build \fluidity\ from source.

\subsection{\fluidity\ binary packages for Ubuntu Linux}

\fluidity\ is distributed via the \lstinline[language=Bash]+fluidity-core+
launchpad package archive. If you run Ubuntu Linux version 12.04 (Precise) or
newer and have administrative privileges on
your computer, you can add the package archive to your system by typing:

\begin{lstlisting}[language=Bash]
sudo apt-add-repository -y ppa:fluidity-core/ppa
\end{lstlisting}

Type your password if prompted. Once the repository has been added, update your
system and install \fluidity\ along with the required supporting software by
typing:

\begin{lstlisting}[language=Bash]
sudo apt-get update
sudo apt-get -y install fluidity
\end{lstlisting}

You now have \fluidity\ installed on your computer. Examples, as referred to in
chapter \ref{chap:examples}, are available as a compressed tarball and can be
expanded into a writeable directory with:

\begin{lstlisting}[language=Bash]
tar -zxvf /usr/share/doc/fluidity/examples.tar.gz
\end{lstlisting}

\subsection{\fluidity\ source packages}

\fluidity\ is released as compressed source archives which can be downloaded
from the \fluidity\ project page on Launchpad,
\href{https://launchpad.net/fluidity}{https://launchpad.net/fluidity} .

You can download just the source, or you can additionally download a tests
archive containing the \fluidity\ short and medium test suite.

To uncompress the \fluidity\ 4.1.3 source from an archive file in Linux,
run:

\begin{lstlisting}[language=Bash]
tar -zxvf fluidity-4.1.3.tar.gz
\end{lstlisting}

This should create a directory, \lstinline[language=Bash]+fluidity-4.1.3/+,
containing your copy of the source code. To add the standard tests (sufficient
to run 'make mediumtest'), download the additional fluidity-tests-4.1.3
archive into the \lstinline[language=Bash]+fluidity-4.1.3/+ directory, and in
that directory run:

\begin{lstlisting}[language=Bash]
tar -zxvf fluidity-tests-4.1.3tar.gz
\end{lstlisting}

The files will appear in a \lstinline[language=Bash]+tests/+ directory.

\subsection{Bazaar}
\label{sec:bazaar}\index{bazaar|primary}\index{bzr|see{bazaar}}

The \fluidity\ source code is hosted on launchpad. Experienced bzr users can
obtain the latest development version of \fluidity\ from
\lstinline[language=Bash]+lp:fluidity+. For inexperienced users, or those
wanting a more stable version than the development trunk, please read on to
learn how to obtain a copy of \fluidity.

As a \fluidity\ user you need only be aware of two modes of operation in
bazaar. The first is checking out a copy of the source code from the
central repository, the second is updating your copy of the code to reflect
changes that have been made to the central repository. Think of the repository
as a central archive of source code held on the Launchpad platform.

There are a few other bazaar features useful for users which will be mentioned
in passing.

Details of how to commit changes to \fluidity\ back into the central repository
are outside the scope of this user manual; if you are transitioning to become a
developer who commits changes back into \fluidity\ please contact an existing
\fluidity\ developer to find out how to get commit privileges.

\subsubsection{Checking out a current copy of \fluidity}
\index{subversion}
\label{sec:subversion_checkout_current}

\fluidity\ can be obtained in two forms: release and trunk.

The first, release, is aimed at general users of \fluidity. Releases are static
versions of the code, and an individual release with a specific version number
will never change content. New versions will be produced whenever there is
sufficient demand for new features to be included. Releaseѕ are very well
tested and do not contain any code known to have bugs. As such, releases are
infrequent so will generally not contain the latest changes. The manual and
other resources are written to support releases.

The second, trunk, is the day to day development version of \fluidity. It is
primarily used by developers and is not recommended for everyday users. Updates
appear many times each day, and the trunk will frequently have periods where
not all tests are passing. Use at your own risk.

To check out a copy of \fluidity\ you can use one of the following two commands
for the release and trunk versions respectively, which are given here in
entirety but will be broken down in the subsequent discussion so you can better
understand them.

Release:
\begin{lstlisting}[language=Bash]
bzr co lp:fluidity/4.1 fluidity/
\end{lstlisting}

Trunk:
\begin{lstlisting}[language=Bash]
bzr co lp:fluidity fluidity/
\end{lstlisting}

The first part of this command calls the bzr program:

\begin{lstlisting}[language=Bash]
bzr
\end{lstlisting}

bzr takes a first argument which describes the mode in which it will run. In
this case, it is going to check out a copy of the code, which abbreviates to
\lstinline[language=Bash]+co+ :

\begin{lstlisting}[language=Bash]
bzr co
\end{lstlisting}

The second argument to bzr describes the location used to check out \fluidity\
from, in this case using the Launchpad site (abbreviated to
\lstinline[language=Bash]+lp+) and the series required (ie,
\lstinline[language=Bash]+fluidity/stable+) :

\begin{lstlisting}[language=Bash]
bzr co lp:fluidity/stable
\end{lstlisting}

This is now a valid command line, and if you ran it would check out a copy of
the stable series of \fluidity\ into a directory
\lstinline[language=Bash]+stable/+ in your current directory. If you
prefer to have the code checked out into a directory called
\lstinline[language=Bash]+fluidity/+, append that directory name to the end of
the command line so bzr checks out the code where you want it to:

\begin{lstlisting}[language=Bash]
bzr co lp:fluidity/stable fluidity/
\end{lstlisting}

Bear in mind that the \fluidity\ check out is of the order of many hundreds of
megabytes and should be checked out onto a file system which has corresponding
amounts of free space. As it checks out, bzr should print progress information,
and when it is complete should clear this information and return to a command
prompt.

If you are asked to check out a different series or branch of \fluidity\ you
will simply need to change the name after \lstinline[language=Bash]+lp:+ and
should be given the relevant information by whoever has requested the checkout.

\subsubsection{Updating your copy of \fluidity}
\index{subversion}
\label{sec:subversion_updating}

This section does not apply to users having checked out a \fluidity\ release.

If you are interested in keeping up to date with the latest developments in
\fluidity\ you will probably want to update your local copy of the \fluidity\
code to reflect changes made to the central source code repository. To do this,
change directory so that you are in the directory which you checked out
\fluidity\ to (in the above case, a directory called
\lstinline[language=Bash]+fluidity/+) and then run bzr's update command, which
is abbreviated to \lstinline[language=Bash]+up+ as per:

\begin{lstlisting}[language=Bash]
bzr up
\end{lstlisting}

You will probably see a number of lines with a leading
\lstinline[language=Bash]+M+, denoting that a file in your local copy of
\fluidity\ is being modified, such as:

\begin{lstlisting}[language=Bash]
M    preprocessor/Populate_State.F90
\end{lstlisting}

This manual assumes that you are not modifying any of the files in your copy of
\fluidity. If you do so, and your changes clash with changes from the central
repository, you may end up with bzr reporting conflicts when you update. This
is worth being aware of, but brings you into the realms of \fluidity\
development, outside the scope of this manual.

\subsubsection{Other useful Bazaar commands}
\index{subversion}
\label{sec:subversion_extras}

If you can check out and update your copy of \fluidity\ then you are equipped to
be an active \fluidity\ user. However, you may wish to know about a few more
useful subversion commands, including getting information about your local
version of the code using:

\begin{lstlisting}[language=Bash]
bzr info
\end{lstlisting}

You may well want to do this if someone asks you which branch of \fluidity\ you
are using, at which point you're looking for a line such as:

\begin{lstlisting}[language=Bash]
checkout of branch: bzr+ssh://bazaar.launchpad.net/%2Bbranch/fluidity/
\end{lstlisting}

with the relevant information. If you are asked for the revision of
\fluidity\ you're using, a shortcut command to give this is
\lstinline[language=Bash]+bzr revision-info+.

To get information on what files have been modified in your
local copy use the the following:
\begin{lstlisting}[language=Bash]
bzr status
\end{lstlisting}

Note that if you have compiled \fluidity\ you will see a large number of files
in the 'unknown' category, which have been generated by the compilation
process. To suppress those supply the parameter \lstinline[language=Bash]+-V+:
\begin{lstlisting}[language=Bash]
bzr status -V
\end{lstlisting}

To get specific information on the difference between a local file and
the central repository use the following:
\begin{lstlisting}[language=Bash]
bzr diff <filename>
\end{lstlisting}

You can get help on the bzr command \lstinline[language=Bash]+<command>+
with:
\begin{lstlisting}[language=Bash]
bzr help <command>
\end{lstlisting}
and list available commands with:
\begin{lstlisting}[language=Bash]
bzr help commands
\end{lstlisting}

replacing \lstinline[language=Bash]+<filename>+ with the file you want
differences reported for.

If you wish to delve deeper into Bazaar operation, an excellent source of
detail is
\href{http://doc.bazaar.canonical.com/bzr.2.4/en/user-guide/index.html}
{http://doc.bazaar.canonical.com/bzr.2.4/en/user-guide/index.html}
which is the official Bazaar User Guide.

\section{Building \fluidity}
\label{sec:building_fluidity}

The build process for \fluidity\ comprises a configuration stage and a compile
stage. In the simplest form, this can be completed with two commands, run in
the directory containing your local source code check out, which is denoted
by \fluiditysourcepath\ in this manual:

\begin{lstlisting}[language=Bash]
cd `\fluiditysourcepath' 
./configure
make
\end{lstlisting}

You may often only wish to perform this basic build, but frequently you will
want more fine-grained control over the configuration procedure or you will
want to perform non-default compilation steps. The following section describes
these procedures.

Note that at this point configuration refers to the build-time configuration
options which define how the \fluidity\ program will be compiled, and do not
refer to configuration of the options that you will run \fluidity\ with once it 
has built. However, presence or lack of features configured at the build stage
may change what is available to you at run time.

It is assumed throughout this section that you are in the top-level directory
of your local copy of the \fluidity\ code for the purposes of describing
configuration and compilation commands.

\subsection{Setting up the build environment}

Depending on the computer you are building \fluidity\ on, you may need to set
up the build environment before configuring \fluidity. On the standard
\fluidity\ workstation built around Ubuntu Linux, you will need to initialise
the PETSc package by running:

\begin{lstlisting}[language=Bash]
module load petsc-gcc4
\end{lstlisting}

This is very specific to the standard \fluidity\ workstation, and other systems
may well have detailed requirements to be carried out before building
\fluidity. If you are in any doubt, stop now, and consult with your local
systems administrator who should be able to help you resolve any problems ahead
of time. If you are not on a standard \fluidity\ workstation you may want to
refer to (or refer your systems administrator to) appendix \ref{chap:external}
of this manual before proceeding.

The build environment is discussed in more detail in sections
\ref{sec:configure_locate_supporting_libs} and
\ref{sec:configure_environment_vars}.

\subsection{Configuring the build process}
\label{sec:configuring_the_build_process}

For a full list of the build-time configuration options available in fluidity,
run:

\begin{lstlisting}[language=Bash]
./configure --help
\end{lstlisting}

Key configuration options are described here, but you are advised to check
output from the above command for any changed or new options which may have
been introduced since the last update of this manual.

Where you wish to specify multiple configuration options at once, supply them
all on the same configuration command line, separated by spaces. For example:

\begin{lstlisting}[language=Bash]
./configure --prefix=/data/fluidity --enable-debugging
\end{lstlisting}

Note that there is one key option NOT enabled by default which users running
the \fluidity\ examples will need to enable to make all examples work. This is 
\lstinline[language=Bash]+--enable-2d-adaptivity+ , which cannot be turned on
as a default as it makes use of external code that is not license-compatible
with all other \fluidity\ codes. Hence, it is a reasonable default state for
you to run:

\begin{lstlisting}[language=Bash]
./configure --enable-2d-adaptivity
\end{lstlisting}

One additional consideration is worth making at this point; do you want to put
any components of the \fluidity build outside the build directory for later
use?  An automatic installation for \fluidity (and also for diamond) is
available, and defaults to using a directory which can (normally) only be
written to with superuser privileges. However, you can tell the install process
to put files elsewhere with the use of the 'prefix' flag to configure. A
typical use of this might be to tell the install process to put files in your
home directory, using the 'HOME' environment variable which should be set by
the system automatically. To do this in addition to enabling 2D adaptivity, you
would type:

\begin{lstlisting}[language=Bash]
./configure --enable-2d-adaptivity --prefix=$HOME
\end{lstlisting}

We'll go on now to discussing enabling and disabling features.

\subsubsection{Enabling and disabling features}
\label{sec:configure_enable_disable_features}

\fluidity\ has a number of optional features which may be enabled or disabled at
build time. For a list of all these features see the output of configuring with
the \lstinline[language=Bash]+--help+ argument. This list should indicate which
options are enabled by default by appending
\lstinline[language=Bash]+(default)+ to the option description. An example of
enabling \fluidity's debugging feature at build time would be:

\begin{lstlisting}[language=Bash]
./configure --enable-debugging
\end{lstlisting}

Whilst a number of options have the facility to be enabled and disabled, doing
so may be prejudicial to the expected normal running of \fluidity\ so unless you
are fully aware of the consequences of enabling or disabling features it is
recommended that you do not do so.

\subsubsection{Specifying locations of supporting libraries}
\label{sec:configure_locate_supporting_libs}

\fluidity\ requires many supporting libraries, which can either be provided via
environment variables (see later discussion for how to do this) or, in specific
cases, provided via options during configuration. This uses the
\lstinline[language=Bash]+--with+ option, for example specifying the directory
containing BLAS using:

\begin{lstlisting}[language=Bash]
./configure --with-blas-dir=/data/libraries/netlib/BLAS
\end{lstlisting}

Whilst there is also the option to supply \lstinline[language=Bash]+--without+
arguments, this is likely to be highly prejudicial to the normal running of
\fluidity\ or, in many cases, be incompatible with building \fluidity\ such that
the configuration exits with an error.

\subsubsection{Environment variables set for configuration}
\label{sec:configure_environment_vars}

A description of environment variables is outside the scope of this manual, and
\fluidity\ users are encouraged to find an experienced UNIX user who can explain
the rudiments of environment variables to them if they are not already familiar
with how to set and use them. Influential environment variables are listed
towards the end of the help output from \fluidity's configuration. Particularly
notable are:

\lstinline[language=Bash]+LIBS+, which allows passing a series of linker
arguments such as \lstinline[language=Bash]+"-L/data/software/libs"+ describing
how to access libraries at link-time. This will often be set or appended to by
loading modules on modern UNIX systems.

\lstinline[language=Bash]+FCFLAGS+ and \lstinline[language=Bash]+FFLAGS+ which
describe flags passed to the Fortran and Fortran77 compilers respectively and
allow you broad control of the overall the build process. This will also often
be set or appended to by loading modules on modern UNIX systems.

\lstinline[language=Bash]+PETSC_DIR+ defines the base directory into which your
PETSc install has been placed. This will often be set by loading an environment
module specific to your system, but if you have a local build of PETSc you may
need to set this variable yourself. Note that for most \fluidity\ users, having
PETSc to provide solver functionality is unavoidable so setting this variable
by some means is necessary in almost all cases. The standard \fluidity\
workstation allows you to do this by running
\lstinline[language=Bash]+module load petsc-gcc4+ .

\lstinline[language=Bash]+VTK_INCLUDE+ and \lstinline[language=Bash]+VTK_LIBS+
may be important to set if VTK is not installed at a system level. VTK is
critical to \fluidity\ for writing output files, and many UNIX systems lack some
VTK components so it frequently ends up installed in a nonstandard location.

\subsection{Compiling \fluidity}
\label{sec:compiling_fluidity}

Once you have successfully configured \fluidity, you need to compile the source
code into binary files including programs and libraries. In the simplest form
you can do this by running:

\begin{lstlisting}[language=Bash]
make
\end{lstlisting}

which will generate \fluidity\ and Diamond programs in the
\lstinline[language=Bash]+bin/+ directory and a set of libraries in the
\lstinline[language=Bash]+lib/+ directory. 

If you have a modern, powerful computer then you can speed this process up
significantly by parallelising the make, running:

\begin{lstlisting}[language=Bash]
make -jN
\end{lstlisting}
where \lstinline[language=Bash]+N+ is the number of CPU cores of your system.

Note that this is very resource-intensive and whilst most modern workstations
should be equal to the task it should not be run on shared machines (ie, login
nodes of compute clusters) or systems with smaller quantities of memory.

If you want to build the extended set of \fluidity\ tools which supplement the
main \fluidity\ program, section \ref{sec:fltools}, run:

\begin{lstlisting}[language=Bash]
make fltools
\end{lstlisting}

If this is the first time you have run \fluidity\ on your computer and you want
to check that it has built correctly, you can run the included suite of test
problems at three levels. The shortest tests test individual units of code for
correctness and can be run with:

\begin{lstlisting}[language=Bash]
make unittest
\end{lstlisting}

Please note that, due to compiler bugs which Intel have shown no inclination to
fix, the 'make unittest' suite is known to fail with all releases of the Intel
compiler. Thus, it is advised that you not run 'make unittest' if building
\fluidity\ with any version of the Intel compiler.

To run the suite of short test cases which more extensively test the
functionality of your \fluidity\ build, run:

\begin{lstlisting}[language=Bash]
make test
\end{lstlisting}

For the most comprehensive set of tests included in your checked out copy of
\fluidity, run:

\begin{lstlisting}[language=Bash]
make mediumtest
\end{lstlisting}

If you have obtained \fluidity\ through source archives rather than through
subversion you will need to have also downloaded and unpacked the standard
tests archive to be able to run \lstinline[language=Bash]+make test+ and
\lstinline[language=Bash]+make mediumtest+ .

Note that even on the most modern systems
\lstinline[language=Bash]+make mediumtest+ may take on the order of an hour and
on slower systems may take on the order of many hours to complete.

\subsection{Installing \fluidity and diamond}
\label{sec:installing_fluidity}

It is perfectly possible to run \fluidity and diamond from inside the build
tree, and many users do just that with appropriate setting of variables.
However, you may want to install at least diamond if not the rest of the
\fluidity package into the directory you had the option of specifying during
the configure stage.

There are three key commands to be aware of here.

The first is:

\begin{lstlisting}[language=Bash]
make install
\end{lstlisting}

This will install \fluidity and a full set of \fluidity tools into the
directory you specified with 'prefix' during configuration. If you didn't
specify a directory there, it will try to install into the
\lstinline[language=Bash]+/usr+ directory, which generally needs superuser
privileges. If you get a lot of 'permission denied' messages, you probably
forgot to specify a prefix (or specified an invalid prefix) at configure time,
and the install is trying to write to a superuser-only or nonexistant
directory. 

Assuming you specified a prefix of \lstinline[language=Bash]+\$HOME+, \fluidity
will install into directories such as \lstinline[language=Bash]+bin/+,
\lstinline[language=Bash]+lib/+, and \lstinline[language=Bash]+share/+ in your
home directory.

\fluidity does not by default install diamond, the graphical configuration tool
used to set up \fluidity. If you do not already have a central installation of
diamond on your computer, you may want to install a copy of it into the
directory you specified with 'prefix' at configure time. To do this, run:

\begin{lstlisting}[language=Bash]
make install-diamond
\end{lstlisting}

which will install diamond into a \lstinline[language=Bash]+bin/+ directory
inside your 'prefix' directory. In the case we've been working through, this
would be your home directory. It will also install supporting python files into
a subdirectory of \lstinline[language=Bash]+lib/+.

To set up your environment for automatic running of diamond, you probably need
to do three things. For this process, we will assume you specified a prefix of
\lstinline[language=Bash]+\$HOME+ to configure.

First, set your path to automatically find the diamond program:

\begin{lstlisting}[language=Bash]
export PATH=$PATH:$HOME/bin
\end{lstlisting}

Next, run:

\begin{lstlisting}[language=Bash]
ls -d $HOME/lib/python*/site-packages/diamond
\end{lstlisting}

This should return something like:

\begin{lstlisting}[language=Bash]
/home/fred/lib/python2.6/site-packages/diamond
\end{lstlisting}

Copy all of the above apart from the trailing
\lstinline[language=Bash]+/diamond+ into the following command, so that you
type (for the above example):

\begin{lstlisting}[language=Bash]
export PYTHONPATH=$PYTHONPATH:/home/fred/lib/python2.6/site-packages
\end{lstlisting}

Finally, you should register \fluidity with diamond for your account by
telling it where to find the \fluidity schema, by typing:

\begin{lstlisting}[language=Bash]
make install-user-schemata
\end{lstlisting}

Note that this points to the current build tree that you are in; if you change
the name of this build tree, or move it, you will need to ŕun the above command
again to register the new location.

\section{Running \fluidity}
\label{sec:running_fluidity}


\subsection{Running \fluidity\ in serial}
\label{sec:running_fluidity_in_serial}

To run \fluidity\ in serial use the following command:

\begin{lstlisting}[language=bash]
`\fluiditysourcepath'/bin/fluidity foo.flml
\end{lstlisting}

Here, \lstinline[language=Bash]+foo.flml+ is a \fluidity\ configuration file
which defines the options for the model. These files are covered in detail in 
Chapter \ref{chap:configuration} and are set up using a simple Graphical User
Interface, Diamond. See section \ref{sec:running_diamond} for more information 
on how to run Diamond.

There are other options that can be passed to the \fluidity\ executable. A full
list can be obtained by running:

\begin{lstlisting}[language=bash]
`\fluiditysourcepath'/bin/fluidity
\end{lstlisting}

This will produce the following output:

\begin{lstlisting}[language=Bash]
Revision: 3575 lawrence.mitchell@ed.ac.uk-20110907125710-d44hcr4no0ev8icc
Compile date: Sep  7 2011 13:59:34
OpenMP Support			no
Adaptivity support		yes
2D adaptivity support		yes
3D MBA support			no
CGAL support			no
MPI support			yes
Double precision		yes
NetCDF support			yes
Signal handling support		yes
Stream I/O support		yes
PETSc support			yes
Hypre support			yes
ARPACK support			yes
Python support			yes
Numpy support			yes
VTK support			yes
Zoltan support			yes
Memory diagnostics		no
FEMDEM support			no
Hyperlight support		no


Usage: fluidity [options ...] [simulation-file]

Options:
 -h, --help
    Help! Prints this message.
 -l, --log
    Create log file for each process (useful for non-interactive testing).
    Sets default value for -v to 2.
 -v <level>, --verbose
    Verbose output to stdout, default level 0
 -p, --profile
    Print profiling data at end of run
    This provides aggregated elapsed time for coarse-level computation
    (Turned on automatically if verbosity is at level 2 or above)
 -V, --version
    Version
\end{lstlisting}

Note that this also gives information on which version is being used, the
build-time configuration options used and a list of command-line options for
\fluidity. 

Running \fluidity\ will produce several output files. See Chapter \ref{chap:visualisation_and_diagnostics}
for more information.

\subsection{Running \fluidity\ in parallel}
\label{sec:running_fluidity_in_parallel}
\index{parallel}

\fluidity\ is a fully-parallel program, capable of running on thousands of
processors.  It uses the Message Passing Interface (MPI) library to communicate
information between processors. Running \fluidity\ in parallel requires that
MPI is available on your system and is properly configured. The terminology for
parallel processing can sometime be confusing.  Here, we use the term
\emph{processor} to mean one physical CPU core and \emph{process} is one part
of a parallel instance of \fluidity. Normally, the number of processors used
matches the number of processes, i.e. if \fluidity\ is split into 256 parts, it
is run on 256 processors.

To run in parallel one must first decompose the mesh. See section
\ref{decomp_meshes_parallel} for more information on how to do this. \fluidity\
must then be run within the mpirun or mpiexec framework. Simply prepend the
normal command line with mpiexec:
\begin{lstlisting}[language=bash]
mpiexec -n [PROCESSES] `\fluiditysourcepath'/bin/fluidity foo.flml
\end{lstlisting}

\section{Running diamond}
\label{sec:running_diamond}
\index{Diamond}

Diamond is the Graphical User Interface (GUI) by which a \fluidity\ simulation
is configured. The flml file that \fluidity\ uses is an XML file, which defines
the meshes, fields and options for \fluidity. Chapter \ref{chap:configuration}
covers the options that are currently available. In addition, Diamond will
display inline help on options where available.

If the \lstinline[language=Bash]+make+ command successfully completed, there
will be an executable program \lstinline[language=Bash]+diamond+ in the
\lstinline[language=Bash]+bin/+ directory. Running diamond is now as simple as
typing:

\begin{lstlisting}[language=Bash]
`\fluiditysourcepath'/bin/diamond
\end{lstlisting}

If the \lstinline[language=Bash]+make install+ command completed and a directory
which executable programs were installed into to the
\lstinline[language=Bash]+PATH+ environment variable, one may also be able to run
diamond simply by typing \lstinline[language=Bash]+diamond+ at the command line
from any directory on your system. This may also be possible if the
Diamond package for Ubuntu or Debian is installed by running:

\begin{lstlisting}[language=Bash]
sudo apt-get install diamond
\end{lstlisting}

Note that installing Diamond on a system-wide basis will require superuser privileges on the system.

\section{Working with the output}
\label{sec:working_with_output}

Once \fluidity\ has been built and run, result files will be generated, that
will need visualising and post-processing. These data are stored in VTK files,
one per output dump.  How these files can be visualised and post-processed is
covered in detail in chapter \ref{chap:visualisation_and_diagnostics}.

As well as visualisation, another important output resource is the stat file,
described in detail in section \ref{sec:diagnostics_stat_file}, which contains
data from the model collected at run time.

See the \href{http://amcg-www.ese.ic.ac.uk/}{AMCG website}\ for more
information.
