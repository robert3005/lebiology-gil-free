#ifndef PATH_OPS_HPP
#define PATH_OPS_HPP

#include <string>
#include <boost/filesystem.hpp>
#include <vector>
#include <fstream>

using namespace std;
using namespace boost::filesystem;

string composePath(vector<string> &);
string parentPath(const string &);
string absolutePath(const string &);
int remove(const string &);
bool create(const string &);
void copy(const string &, const string &);
void writeFile(const string &, const vector<string> &);
void writeFile(const string &, const string &);
string readFile(const string &);

#endif
