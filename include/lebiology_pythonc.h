#include "confdefs.h"

#ifdef HAVE_PYTHON
#include "Python.h"
#include "lebiology.h"
#endif

extern "C" {
/* setup lebiology module */
void lebiology_init(int *stat);

/* Update agent biology from a Python function
 * Usage: def val(agent, env, dt):
 *          agent['var'] = f( env['field'], dt )
 *          return agent
 */
void lebiology_agent_update_c(char *fg, char *key, char *food, double vars[], int n_vars, double envvals[], int n_envvals,
                              double fvariety[], double frequest[], double fthreshold[], double fingest[], int n_fvariety,
                              double *dt, int *dropout, int *stat);

}
