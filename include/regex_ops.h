#ifndef REGEX_OPS_HPP
#define REGEX_OPS_HPP

#include <vector>
#include <string>

using namespace std;

vector<string> * findImportedModules(const string &);

#endif
