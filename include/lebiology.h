#ifndef __PYX_HAVE__lagrangian_biology__lebiology
#define __PYX_HAVE__lagrangian_biology__lebiology


#ifndef __PYX_HAVE_API__lagrangian_biology__lebiology

#ifndef __PYX_EXTERN_C
  #ifdef __cplusplus
    #define __PYX_EXTERN_C extern "C"
  #else
    #define __PYX_EXTERN_C extern
  #endif
#endif

__PYX_EXTERN_C DL_IMPORT(void) lebiology_setup(char *, int);
__PYX_EXTERN_C DL_IMPORT(void) lebiology_add_fg_varname_c(char *, char *, int *);
__PYX_EXTERN_C DL_IMPORT(void) lebiology_add_fg_envname_c(char *, char *, int *);
__PYX_EXTERN_C DL_IMPORT(void) lebiology_add_fg_foodname_c(char *, char *, char *, int *);
__PYX_EXTERN_C DL_IMPORT(void) lebiology_add_fg_stage_id_c(char *, char *, double *, int *);
__PYX_EXTERN_C DL_IMPORT(void) lebiology_create_compile_dir_c(int *);
__PYX_EXTERN_C DL_IMPORT(void) lebiology_create_compile_dir_fgroup_c(char *, int *);
__PYX_EXTERN_C DL_IMPORT(void) lebiology_compile_function_c(char *, char *, char *, int *);
__PYX_EXTERN_C DL_IMPORT(void) lebiology_compile_ext(void);
__PYX_EXTERN_C DL_IMPORT(void) lebiology_agent_init_c(char *, double *, int, int *);
__PYX_EXTERN_C DL_IMPORT(void) lebiology_agent_update_c(char *, char *, char *, double *, int, double *, int, double *, double *, double *, double *, int, double *, int *, int *);

#endif /* !__PYX_HAVE_API__lagrangian_biology__lebiology */

#if PY_MAJOR_VERSION < 3
PyMODINIT_FUNC initlebiology(void);
#else
PyMODINIT_FUNC PyInit_lebiology(void);
#endif

#endif /* !__PYX_HAVE__lagrangian_biology__lebiology */
